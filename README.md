# SimplySprint

Want to sprint in any direction? Maybe just a slightly wider or smaller angle? Or do you simply want to adjust the movement and sprinting speeds? SimplySprint is for you!

## Features:

-   `OmnidirectionalSprinting` - Sprint in any direction!
-   `[SprintingAngleCutoff]` - Adjust the angle at which the game lets you sprint!
-   `[Speed]` - Adjust your (and your enemies'!) speed
    -   `BaseSpeedMultiplier`/`SprintingSpeedMultiplier` - Multiply the speed at which you move/sprint!
    -   `ApplyBaseSpeedOnNPCs`/`ApplySprintingSpeedOnNPCs` - Enable those to adjust the speed at which enemies move/sprint in the same way players do!
-   `[ProgressiveSprinting]` - 2fast4you? You can start out slow and go to full speed over time!
-   `[MoveSpeedCap]` - You can limit the maximum speed stat the player can reach - no more uncontrollably flying off into the sky!

### Note on compatibility:

If using with [RT AutoSprint](https://thunderstore.io/package/Relocity3E8/RT_AutoSprint/), enable `OverwriteStaticField` for compatibility.

## Installation

Copy the `SimplySprint` folder to `Risk of Rain 2/BepInEx/plugins`

## Patch notes:

- 1.4.0
    - Update for Survivors of the Void
- 1.2.0
    - Add options to toggle affecting enemy speeds
    - Add option to adjust base movement speed
    - Moved `SprintingSpeedMultiplier` to new section - don't worry, it gets automigrated!
- 1.1.2 - Added optional speed cap
- 1.1.1 - Added compatibility with ModRecalculate, fixed progressive sprinting not affecting soda can properly
- 1.1.0 - Added progressive sprinting
- 1.0.0 - Initial release