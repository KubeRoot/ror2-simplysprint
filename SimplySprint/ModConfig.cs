﻿using BepInEx.Configuration;
using System.Collections.Generic;

namespace SimplySprint
{
    public static class ModConfig
    {
        private static ConfigFile config;

        private static System.Reflection.PropertyInfo OrphanedEntriesProperty = typeof(ConfigFile).GetProperty("OrphanedEntries", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);

        private static void MigrateConfig()
        {
            if(OrphanedEntriesProperty == null)
            {
                UnityEngine.Debug.Log("SimplySprint failed to migrate properties - reflection failed");
                return;
            }
            Dictionary<ConfigDefinition, string> orphanedEntries = (Dictionary<ConfigDefinition, string>)OrphanedEntriesProperty.GetValue(config);

            List<(ConfigDefinition, ConfigDefinition)> entriesToMigrate = new List<(ConfigDefinition, ConfigDefinition)> {
                (new ConfigDefinition("General", "SprintingSpeedMultiplier"), new ConfigDefinition("Speed", "SprintingSpeedMultiplier"))
            };
            
            foreach(var (from, to) in entriesToMigrate)
            {
                if(orphanedEntries.ContainsKey(from))
                {
                    orphanedEntries.Add(to, orphanedEntries[from]);
                    orphanedEntries.Remove(from);
                }
            }
        }

        internal static void InitConfig(ConfigFile _config)
        {
            config = _config;

            MigrateConfig();

            overrideMinAimMoveDot = config.Bind("General", "OverwriteStaticField", false, "Should the mod write to the sprintMinAimMoveDot field directly?\nPotentially less compatible/robust, can help with compatibility with some mods");
            omnidirectionalSprinting = config.Bind("General", "OmnidirectionalSprinting", false, "Should players be able to sprint in any direction?");
            
            applySprintingSpeedOnNPC = config.Bind("Speed", "ApplySprintingSpeedOnNPCs", false, "Should the sprinting speed multiplier affect enemies?");
            sprintingSpeedMultiplier = config.Bind("Speed", "SprintingSpeedMultiplier", 1f, "Additional multiplier to speed while sprinting (stacks with builtin multiplier, which is 1.45)");
            applyBaseSpeedOnNPC = config.Bind("Speed", "ApplyBaseSpeedOnNPCs", false, "Should the base speed multiplier affect enemies?");
            baseSpeedMultiplier = config.Bind("Speed", "BaseSpeedMultiplier", 1f, "Multiplier to speed, works even when not sprinting");

            overrideSprintingAngleCutoff = config.Bind("SprintingAngleCutoff", "Enabled", false, "Should the angle cutoff at which the player can no longer sprint be overriden? (Separate from OmnidirectionalSprinting)");
            sprintingAngleCutoff = config.Bind(new ConfigDefinition("SprintingAngleCutoff", "NewAngleCutoff"), 60f, new ConfigDescription("Maximum angle at which the player should be able to sprint, in degrees", new AcceptableValueRange<float>(0, 180)));

            void UpdateSprintingAngle()
            {
                sprintingAngleCutoffDot = UnityEngine.Mathf.Cos(sprintingAngleCutoff.Value * UnityEngine.Mathf.PI / 180f);
            }
            
            sprintingAngleCutoff.SettingChanged += (_, __) =>
            {
                UpdateSprintingAngle();
            };
            UpdateSprintingAngle();

            enableProgressiveSprinting = config.Bind("ProgressiveSprinting", "Enabled", false, "Should the player speed up over time when starting to sprint?");
            progressiveSprintingDuration = config.Bind("ProgressiveSprinting", "Duration", 1f, "Time in seconds until the player starts moving at full speed (0-)");

            enableSpeedCap = config.Bind("MoveSpeedCap", "Enabled", false, "Should the player's speed be capped?");
            movementSpeedCap = config.Bind("MoveSpeedCap", "Cap", 30f, "Maximum speed stat the player can achieve (0-)");
        }

        public static ConfigEntry<bool> omnidirectionalSprinting;
        public static ConfigEntry<bool> overrideMinAimMoveDot;

        public static ConfigEntry<bool> applyBaseSpeedOnNPC;
        public static ConfigEntry<float> baseSpeedMultiplier;
        public static ConfigEntry<bool> applySprintingSpeedOnNPC;
        public static ConfigEntry<float> sprintingSpeedMultiplier;

        public static ConfigEntry<bool> enableSpeedCap;
        public static ConfigEntry<float> movementSpeedCap;

        public static ConfigEntry<bool> overrideSprintingAngleCutoff;
        public static ConfigEntry<float> sprintingAngleCutoff;
        public static float sprintingAngleCutoffDot
        {
            get;
            private set;
        }

        public static ConfigEntry<bool> enableProgressiveSprinting;
        public static ConfigEntry<float> progressiveSprintingDuration;
    }
}
