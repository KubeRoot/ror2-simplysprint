﻿using RoR2;

namespace SimplySprint
{
    class ProgressiveSprintingBehavior : UnityEngine.MonoBehaviour
    {
        private CharacterBody body;
        public float SprintingDuration
        {
            get;
            private set;
        }
        private bool wasSprinting;

        void FixedUpdate()
        {
            if (!ModConfig.enableProgressiveSprinting.Value)
                return;

            bool shouldRecalculate = false;

            if(body.isSprinting)
            {
                if (SprintingDuration < ModConfig.progressiveSprintingDuration.Value)
                    shouldRecalculate = true;

                SprintingDuration += UnityEngine.Time.fixedDeltaTime;
            }
            else
            {
                SprintingDuration = 0f;
                if (wasSprinting)
                    shouldRecalculate = true;
            }

            if (shouldRecalculate)
                body.RecalculateStats();

            wasSprinting = body.isSprinting;
        }

        void Awake()
        {
            SprintingDuration = 0f;
            body = GetComponent<CharacterBody>();
            if (!body)
                Destroy(this);
        }
    }
}
