﻿using BepInEx;
using MonoMod.Cil;
using MonoMod.RuntimeDetour.HookGen;
using RoR2;
using System;
using System.Reflection;
using UnityEngine;

namespace SimplySprint
{
    [BepInDependency("com.bepis.r2api")]
    [BepInDependency("com.Plexus.ModRecalculate", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInPlugin("com.kuberoot.simplysprint", "SimplySprint", "1.4.0")]
    // Technically SimplySprint does not do any synchronization and generally seems to work when only one person has the mod... but it might effectively function as a cheat mod, so we make sure everybody has it
    [R2API.Utils.NetworkCompatibility(R2API.Utils.CompatibilityLevel.EveryoneMustHaveMod, R2API.Utils.VersionStrictness.DifferentModVersionsAreOk)]
    public class SimplySprintPlugin : BaseUnityPlugin
    {
        private static FieldInfo sprintMinAimMoveDotField = typeof(PlayerCharacterMasterController).GetField("sprintMinAimMoveDot", BindingFlags.NonPublic | BindingFlags.Static);

        private float sprintMinAimMoveDotFieldOldValue;

        public void OnEnable()
        {
            ModConfig.InitConfig(Config);

            PlayerCharacterMasterController.onPlayerAdded += OnPlayerAdded;

            IL.RoR2.PlayerCharacterMasterController.FixedUpdate += ModifyAimMoveDot;
            IL.RoR2.CharacterBody.RecalculateStats += RecalculateStatsHook;

            On.RoR2.CharacterBody.Awake += AttachProgressiveSprintingBehavior;

            sprintMinAimMoveDotFieldOldValue = (float)sprintMinAimMoveDotField.GetValue(null);
            if(ModConfig.overrideMinAimMoveDot.Value)
            {
                sprintMinAimMoveDotField.SetValue(null, ModConfig.omnidirectionalSprinting.Value ? -2f : ModConfig.sprintingAngleCutoff.Value);
            }
        }

        public void OnDisable()
        {
            HookEndpointManager.RemoveAllOwnedBy(HookEndpointManager.GetOwner((Action<ILContext>)ModifyAimMoveDot));
            sprintMinAimMoveDotField.SetValue(null, sprintMinAimMoveDotFieldOldValue);
        }

        private void AttachProgressiveSprintingBehavior(On.RoR2.CharacterBody.orig_Awake orig, CharacterBody self)
        {
            orig(self);

            self.gameObject.AddComponent<ProgressiveSprintingBehavior>();
        }

        private void ModifyAimMoveDot(ILContext il)
        {
            ILCursor cursor = new ILCursor(il);

            cursor.GotoNext(MoveType.After, i => i.MatchLdsfld<PlayerCharacterMasterController>("sprintMinAimMoveDot"));
            cursor.EmitDelegate<Func<float, float>>(i =>
            {
                return (!ModConfig.overrideMinAimMoveDot.Value && ModConfig.overrideSprintingAngleCutoff.Value) ? ModConfig.sprintingAngleCutoffDot : i;
            });
        }
        
        private void RecalculateStatsHook(ILContext il)
        {
            MultiplySprintSpeed(il);
            MultiplyBaseSpeed(il);
        }

        private void MultiplyBaseSpeed(ILContext il)
        {
            ILCursor cursor = new ILCursor(il);

            cursor.GotoNext(MoveType.Before, i => i.MatchCallOrCallvirt<CharacterBody>("set_moveSpeed"));
            cursor.Emit(Mono.Cecil.Cil.OpCodes.Ldarg_0);
            cursor.EmitDelegate<Func<float, CharacterBody, float>>((i, self) =>
            {
                if (self.isPlayerControlled || ModConfig.applyBaseSpeedOnNPC.Value)
                    return i * ModConfig.baseSpeedMultiplier.Value;
                return i;
            });
        }

        private void MultiplySprintSpeed(ILContext il)
        {
            ILCursor cursor = new ILCursor(il);
            
            cursor.GotoNext(MoveType.After, i => i.MatchLdfld<CharacterBody>("sprintingSpeedMultiplier"));
            cursor.Emit(Mono.Cecil.Cil.OpCodes.Ldarg_0);
            cursor.EmitDelegate<Func<float, CharacterBody, float>>((i, self) => {
                if (self.isPlayerControlled || ModConfig.applySprintingSpeedOnNPC.Value)
                    return i * ModConfig.sprintingSpeedMultiplier.Value;
                return i;
            });

            cursor.GotoNext(MoveType.Before, i => i.MatchMul());
            cursor.Emit(Mono.Cecil.Cil.OpCodes.Ldarg_0);
            cursor.EmitDelegate<Func<float, CharacterBody, float>>((i, self) =>
            {
                if (ModConfig.enableProgressiveSprinting.Value)
                {
                    ProgressiveSprintingBehavior progressiveSprintingBehavior = self.GetComponent<ProgressiveSprintingBehavior>();

                    if (progressiveSprintingBehavior == null)
                        return i;

                    float mult = Math.Max(0f, Math.Min(1f, progressiveSprintingBehavior.SprintingDuration / ModConfig.progressiveSprintingDuration.Value));

                    return i * mult + 1 - mult;
                }
                else
                    return i;
            });
            
            cursor.GotoNext(i => i.MatchLdfld<CharacterBody>("sprintingSpeedMultiplier"));
            cursor.GotoNext(MoveType.Before, i => i.MatchAdd());
            cursor.Emit(Mono.Cecil.Cil.OpCodes.Ldarg_0);
            cursor.EmitDelegate<Func<float, CharacterBody, float>>((i, self) =>
            {
                if (ModConfig.enableProgressiveSprinting.Value)
                {
                    ProgressiveSprintingBehavior progressiveSprintingBehavior = self.GetComponent<ProgressiveSprintingBehavior>();

                    if (progressiveSprintingBehavior == null)
                        return i;

                    float mult = Math.Max(0f, Math.Min(1f, progressiveSprintingBehavior.SprintingDuration / ModConfig.progressiveSprintingDuration.Value));

                    return i * mult;
                }
                else
                    return i;
            });
            
            cursor.GotoNext(i => i.MatchCallOrCallvirt<CharacterBody>("set_moveSpeed"));
            cursor.Emit(Mono.Cecil.Cil.OpCodes.Ldarg_0);
            cursor.EmitDelegate<Func<float, CharacterBody, float>>((i, self) =>
            {
                if (ModConfig.enableSpeedCap.Value)
                {
                    if(self.isPlayerControlled)
                    {
                        i = Math.Min(i, ModConfig.movementSpeedCap.Value);
                    }
                    return i;
                }
                else
                    return i;
            });
        }

        private void MultiplySprintSpeed_ModRecalculate_0_5_1(ILContext il)
        {
            ILCursor cursor = new ILCursor(il);
            cursor.GotoNext(i => i.MatchLdstr("sprintingSpeedMultiplier"));
            cursor.GotoNext(MoveType.Before, i => i.MatchMul());

            cursor.EmitDelegate<Func<float, float>>(i => i * ModConfig.sprintingSpeedMultiplier.Value);
            cursor.Emit(Mono.Cecil.Cil.OpCodes.Ldarg_0);
            cursor.EmitDelegate<Func<float, CharacterBody, float>>((i, self) =>
            {
                if (ModConfig.enableProgressiveSprinting.Value)
                {
                    ProgressiveSprintingBehavior progressiveSprintingBehavior = self.GetComponent<ProgressiveSprintingBehavior>();

                    if (progressiveSprintingBehavior == null)
                        return i;

                    float mult = Math.Max(0f, Math.Min(1f, progressiveSprintingBehavior.SprintingDuration / ModConfig.progressiveSprintingDuration.Value));

                    return i * mult + 1 - mult;
                }
                else
                    return i;
            });

            for (int j = 0; j < 2; j++)
            {
                cursor.GotoNext(i => i.MatchCallOrCallvirt<CharacterBody>("get_isSprinting"));
                cursor.GotoNext(MoveType.Before, i => i.MatchAdd());
                cursor.Emit(Mono.Cecil.Cil.OpCodes.Ldarg_0);
                cursor.EmitDelegate<Func<float, CharacterBody, float>>((i, self) =>
                {
                    if (ModConfig.enableProgressiveSprinting.Value)
                    {
                        ProgressiveSprintingBehavior progressiveSprintingBehavior = self.GetComponent<ProgressiveSprintingBehavior>();

                        if (progressiveSprintingBehavior == null)
                            return i;

                        float mult = Math.Max(0f, Math.Min(1f, progressiveSprintingBehavior.SprintingDuration / ModConfig.progressiveSprintingDuration.Value));

                        return i * mult;
                    }
                    else
                        return i;
                });
            }
        }

        private void OnPlayerAdded(PlayerCharacterMasterController player)
        {
            player.master.onBodyStart += OnBodyStart;
        }

        private void OnBodyStart(CharacterBody body)
        {
            if(!ModConfig.overrideMinAimMoveDot.Value && ModConfig.omnidirectionalSprinting.Value)
            {
                body.bodyFlags |= CharacterBody.BodyFlags.SprintAnyDirection;
            }
        }
    }
}